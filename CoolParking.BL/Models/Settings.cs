﻿using System.Collections.Generic;

public static class Settings
{
    public static int InitialBalanceOfParking { get; } = 0;
    public static int ParkingCapacity { get; } = 10;
    public static int PaymenPeriod { get; } = 2;
    public static int LoggingPeriod { get; } = 10;
    public static decimal FineCoefficient { get; } = 2m;

    public static Dictionary<VehicleType, decimal> Tariffs = new Dictionary<VehicleType, decimal>()
    {
        { VehicleType.PassengerCar, 2 },
        { VehicleType.Truck, 5 },
        { VehicleType.Motorcycle, 1 },
        { VehicleType.Bus, 3.5m },
    };
}