﻿using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace TesterApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ITimerService aTimer = new TimerService(2000);
            aTimer.Elapsed += OnTimedEvent;
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("The Elapsed event was raised at {0:HH:mm:ss.fff}",
                              e.SignalTime);
        }
    }
}
